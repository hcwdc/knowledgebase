package com.castle.fortress.admin.check.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TmpParseData {
    private List<ParseData> parseData;
}
