package com.castle.fortress.admin.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.castle.fortress.admin.member.entity.MemberPointsEntity;
/**
 * 会员积分表Mapper 接口
 *
 * @author Mgg
 * @since 2021-11-27
 */
public interface MemberPointsMapper extends BaseMapper<MemberPointsEntity> {




}

