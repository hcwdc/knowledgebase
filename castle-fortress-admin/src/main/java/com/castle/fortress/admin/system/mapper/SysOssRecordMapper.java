package com.castle.fortress.admin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.castle.fortress.admin.system.entity.SysOssRecordEntity;
/**
 * oss上传记录Mapper 接口
 *
 * @author castle
 * @since 2022-03-01
 */
public interface SysOssRecordMapper extends BaseMapper<SysOssRecordEntity> {




}

