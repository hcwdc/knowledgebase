package com.castle.fortress.admin.log.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.castle.fortress.admin.log.entity.LogOpenapiEntity;
/**
 * 对外开放api调用日志Mapper 接口
 *
 * @author castle
 * @since 2021-04-01
 */
public interface LogOpenapiMapper extends BaseMapper<LogOpenapiEntity> {




}

