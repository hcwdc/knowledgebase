package com.castle.fortress.admin.core.constants;

/**
 * 全局常量 redisKey
 * @author castle
 */
public interface RedisKeyConstants {
    /**
     * 地区树
     */
    String REGION_TREE = "regionTree";


}
