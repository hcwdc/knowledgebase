package com.castle.fortress.admin.log.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.castle.fortress.admin.log.entity.LogJobEntity;
/**
 * 定时任务调用日志Mapper 接口
 *
 * @author castle
 * @since 2021-04-02
 */
public interface LogJobMapper extends BaseMapper<LogJobEntity> {




}

