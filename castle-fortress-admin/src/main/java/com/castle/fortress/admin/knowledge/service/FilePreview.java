package com.castle.fortress.admin.knowledge.service;

import org.springframework.ui.Model;

public interface FilePreview {
    String filePreviewHandle(Model model,String filePath);
}
