package com.castle.fortress.admin.member.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.castle.fortress.admin.member.entity.MemberAccountEntity;
/**
 * 会员账户表Mapper 接口
 *
 * @author Mgg
 * @since 2021-12-02
 */
public interface MemberAccountMapper extends BaseMapper<MemberAccountEntity> {




}

