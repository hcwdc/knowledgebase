package com.castle.fortress.admin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.castle.fortress.admin.system.entity.SysCaptchaEntity;
/**
 * 手机验证码Mapper 接口
 *
 * @author castle
 * @since 2021-07-13
 */
public interface SysCaptchaMapper extends BaseMapper<SysCaptchaEntity> {




}

