package com.castle.fortress.admin.knowledge.model;

import lombok.Data;

@Data
public class FileAttribute {

    private String suffix;
    private String name;
    private String url;
    private String fileKey;
    private String filePassword;
    private String userToken;


}
