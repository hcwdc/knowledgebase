package com.castle.fortress.admin.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.castle.fortress.admin.system.entity.DevViewListConfig;

/**
 * 开发 表字段 配置服务类
 * @author castle
 */
public interface DevViewListConfigService extends IService<DevViewListConfig> {

}
