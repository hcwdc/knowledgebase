package com.castle.fortress.admin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.castle.fortress.admin.system.entity.ConfigApiEntity;
/**
 * 框架绑定api配置管理Mapper 接口
 *
 * @author castle
 * @since 2022-04-12
 */
public interface ConfigApiMapper extends BaseMapper<ConfigApiEntity> {




}

