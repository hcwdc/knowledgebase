package com.castle.fortress.admin.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.castle.fortress.admin.system.entity.SysUserRole;

import java.util.ArrayList;
import java.util.List;

/**
 * @author castle
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
