package com.castle.fortress.develop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.castle.fortress.develop.entity.DevDbConfig;

public interface DevDbConfigMapper extends BaseMapper<DevDbConfig> {
}
