package com.castle.fortress.develop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.castle.fortress.develop.entity.SysMenu;

/**
 * 系统菜单
 * @author castle
 */
public interface SysMenuMapper extends BaseMapper<SysMenu> {

}
