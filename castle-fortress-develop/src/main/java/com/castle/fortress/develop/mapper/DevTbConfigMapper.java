package com.castle.fortress.develop.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.castle.fortress.develop.entity.DevTbConfig;

public interface DevTbConfigMapper extends BaseMapper<DevTbConfig> {
}
