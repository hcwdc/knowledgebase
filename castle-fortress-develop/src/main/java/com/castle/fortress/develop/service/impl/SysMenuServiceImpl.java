package com.castle.fortress.develop.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.castle.fortress.develop.entity.SysMenu;
import com.castle.fortress.develop.mapper.SysMenuMapper;
import com.castle.fortress.develop.service.SysMenuService;
import org.springframework.stereotype.Service;

/**
 * 系统菜单服务实现类
 * @author castle
 */
@Service
public class SysMenuServiceImpl extends ServiceImpl<SysMenuMapper, SysMenu> implements SysMenuService {

}
