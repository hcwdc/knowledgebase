package com.castle.fortress.develop.common;

/**
 * 开发模块常量
 * @author castle
 */
public class DevConstant {
    /**
     * 默认页码
     */
    public static Integer DEFAULT_PAGE_INDEX = 1;
    /**
     * 页面默认显示记录数
     */
    public static Integer DEFAULT_PAGE_SIZE = 20;
}
