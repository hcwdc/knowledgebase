package com.castle.fortress.develop.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.castle.fortress.develop.entity.SysMenu;

/**
 * 系统菜单服务类
 * @author castle
 */
public interface SysMenuService extends IService<SysMenu> {

}
